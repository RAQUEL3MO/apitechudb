package com.techu.apitechudb.models;

import java.util.Map;

public class PurchaseModel {
    private String id;
    private String userid;
    private float amount;
    private Map<String, Integer> purchaseitems;

    public PurchaseModel(){
    }

    public PurchaseModel(String id, String userid, float amount, Map<String, Integer> purchaseitems) {
        this.id = id;
        this.userid = userid;
        this.amount = amount;
        this.purchaseitems = purchaseitems;

    }

    public String getId() {return id;}

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getPurchaseitems() {
        return purchaseitems;
    }

    public void setPurchaseitems(Map<String, Integer> purchaseitems) {
        this.purchaseitems = purchaseitems;
    }
}