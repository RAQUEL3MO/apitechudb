package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.ProductService;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")

public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(value = "age",defaultValue = "0") int age) {
        System.out.println("getUsers");

        return new ResponseEntity<>(
                this.userService.findAll(age),
                HttpStatus.OK
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del user a crear es " + user.getId());
        System.out.println("El name del user a crear es " + user.getName());
        System.out.println("El Age del user a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }


    @GetMapping("/users/{id}")
    public  ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del user a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "User no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user,@PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del user que se va a actualizar en parámetro es " + id);
        System.out.println("La id del user que se va a actualizar es " + user.getId());
        System.out.println("El name del user que se va a actualizar " + user.getName());
        System.out.println("El Age del user que se va a actualizar " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("la id del user a borrar " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "User borrado" : "User no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

//    @GetMapping("/users/ages/{age}")
//    public ResponseEntity<List<UserModel>> getUserByAge(@RequestParam(value = "age",defaultValue = "0") int age){
//        System.out.println("getUserByAge");
//        System.out.println("La age del user a buscar es " + age);
//
//        List<UserModel> result = this.userService.findByAge(age);
//
//        return new ResponseEntity<List<UserModel>>(
//                (List<UserModel>) (result.isEmpty() ? result.get(age) : "User no encontrado"),
//                result.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK
//        );
//
//    }

}
