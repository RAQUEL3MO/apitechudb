package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v4")

public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;


    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchases");

        return new ResponseEntity<>(
                this.purchaseService.findAll(),HttpStatus.OK
        ) ;
    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("addPurchase");
        System.out.println("La id de la compra a crear es " + purchase.getId());
        System.out.println("El id del u suario a crear es " + purchase.getUserid());
        System.out.println("Los elementos de  la compra  a crear es " + purchase.getPurchaseitems());

        PurchaseServiceResponse purchaseServiceResponse = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                purchaseServiceResponse.getPurchase(),
                purchaseServiceResponse.getResponseHttpStatusCode()

        );
    }

}
